from constructItLinesd import construct_it_lines
import matplotlib.pyplot as plt


def ler_dados_graf_scatter() -> dict:
    scatter = {"concelho": [], "casos_14dias": [], "population_65_mais": []}
    lines = construct_it_lines()
    for count, line in enumerate(lines):
        if count < 100:
            scatter["concelho"].append(line["concelho"])
            scatter["casos_14dias"].append(int(line["casos_14dias"]))
            scatter["population_65_mais"].append(int(line["population_65_mais"]))
        else:
            break
    return scatter


"""
produz um gráfico de dispersão em que no eixo das ordenadas temos o nº de casos nos ultimos 14 dias
e nas abcissas o nº de habitantes com 65 anos ou mais. cada ponto corresponde a 1 concelho
"""


def produzir_grafico_dispersao():
    dados = ler_dados_graf_scatter()
    plt.scatter(dados["population_65_mais"], dados["casos_14dias"])
    plt.show()


def main():
    produzir_grafico_dispersao()


main()
