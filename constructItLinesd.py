from requests import get
from io import StringIO
from csv import DictReader
import matplotlib.pyplot as plt


def construct_it_lines():
    url = 'https://raw.githubusercontent.com/dssg-pt/covid19pt-data/master/data_concelhos_new.csv'
    req = get(url, stream=True)
    buffer = StringIO(req.text)
    return DictReader(buffer, delimiter=',')



